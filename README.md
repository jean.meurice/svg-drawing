# SVG Drawing

> **Note**: Work In Progress

## TODOs

- [ ] Separate minimal_xml crate?
  - [ ] To/From memory?
  - [ ] Weak-ref based?
  - [ ] Re-visit API
  - [ ] Structure crate?
  - [ ] Documentation
  - [ ] Tests
- [ ] [SVG Transformations & Coordinate Systems](https://www.sarasoueidan.com/blog/svg-coordinate-systems/)
- [ ] add_svg_text_3d()
- [ ] add_circle_3d()
- **Ideas**
  - [ ] Demo: generate random isometric landscape/pattern? + Include color animations?
    - Pipeline: 
      - Generate landscape data
      - Generate Triangles/Rects (vertices & faces)
      - Project
      - Clip (screen space & Normal direction)
      - Collect & Sort by Z Back to front
      - Shade and generate SVG (color, animations, ...)
    - Use functional programming? (Lambdas with tuples)
  - [ ] Demo: Animated triangle pattern
    - Idea: transparent layer for "wave effect" over the rest
  - [ ] Demo: Generate technical drawing from 3D Model
    - Process geometry
      - Extract meaningful edges from model? (Merge plane aligned triangles?)
        - Or use OBJ with polygons
      - Merge vertices, merge edges (hashmaps)
      - Detect "hidden edges"? (only based on the normals of the adjacent faces in screen space?)
      - Prune edges if aligned to camera
    - Have multiple views: isometric, top, front, bottom
    - Different labels
    - Length notations ? (Helper functions?)
  - [ ] "Color Type" ? Or just "Color functions" that generate Strings? Or ColorType with "Into String" trait?
  - [ ] "Size type" (px, em, ...) with automatic transformations? or just helper function 

## Running Test

```rust
cargo run --bin gen_test
```

## New Concept

- **Question**
  - Allow "floating sub-trees"? (Not created from a parent?) -> **No at first**
- Based on the 'xml' crate for creating an xml tree based on node ids?

- Generic attribute based -> Helper (SVG) functions only set attributes
  - Builders? CircleBuilder, ...
- Transform system
  - Helper functions for *transform attributes*
  - 3D Transform system (module)
  - "Add layer" with own transform
- Type system?
  - Circle, SVG, Group, ... + Into *XML_NODE* ?

## Transform System Discussion

- Transforming Coordinates vs transforming the canvas
  - Transforming the Canvas (viewBox, transform)
    - Problems:
      - needs top-level groups to transform everything
      - Having "labels" on the canvas: will be transformed
  - Having a "projection" from World space to Canvas
    - Advantages:
      - Get text positions but do not rotate the text
      - Conceptually: separate transformations from canvas/SVG specific things
    - Problems
      - "Rect" only allows "orthogonal transformations"
        - Fix: Use a path every time
      - Cannot rotate texts & elipses
        - If actually needed: handle explicitely with 'transform' attributes after transforming the coordinates
  - Having "mixed concepts": not ideal? (not goal of this lib)
- Implementing
  - "View Object" from transformations?
    - Can be transformed further, returns new "View Object"
  - Has mutable ref to the Canvas?
    - Does that work with nested transforms?
  - Same "syntax" as default SVG object, but for transformed elems
    - Special cases: rotated text, normal text, rectangle (use path instead?), handling elements "to the side" (width/height = 0)      

## Example Usage

```rust

```
