# SVG Format

## Minimal File

```xml
<svg version="1.1"
     width="300" height="200"
     xmlns="http://www.w3.org/2000/svg">

  <rect width="100%" height="100%" fill="red" />

  <circle cx="150" cy="100" r="80" fill="green" />

  <text x="150" y="125" font-size="60" text-anchor="middle" fill="white">SVG</text>

</svg>
```

- **SVG** element
  - version, xmlns
  - width, height (unit?)
  - viewport

## Styles

The `<style>` tag can be used to set CSS-style styling based on tag name, classes or ids:

```xml

```

Behavior:

- `<style>` is *global*: it doesn't matter where it is declared (groups and order), it will apply to the entire svg.
- `<style>` overrides individual styling attributes
