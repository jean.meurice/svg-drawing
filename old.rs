use svg::node::element::path::Data;
use svg::node::element::{Circle, Group, Path};
use svg::Document;
use std::io;

use glm::*;

pub mod tiny_xml;

pub fn proj_z() -> DMat4 {
    num::one()
}
pub fn proj_z_minus() -> DMat4 {
    DMat4::new(
        dvec4(-1.0, 0.0, 0.0, 0.0),
        dvec4(0.0, 1.0, 0.0, 0.0),
        dvec4(0.0, 0.0, -1.0, 0.0),
        dvec4(0.0, 0.0, 0.0, 1.0),
    )
}
pub fn proj_y() -> DMat4 {
    DMat4::new(
        dvec4(1.0, 0.0, 0.0, 0.0),
        dvec4(0.0, 0.0, 1.0, 0.0),
        dvec4(0.0, -1.0, 0.0, 0.0),
        dvec4(0.0, 0.0, 0.0, 1.0),
    )
}
pub fn proj_x() -> DMat4 {
    DMat4::new(
        dvec4(0.0, 0.0, -1.0, 0.0),
        dvec4(0.0, 1.0, 0.0, 0.0),
        dvec4(1.0, 0.0, 0.0, 0.0),
        dvec4(0.0, 0.0, 0.0, 1.0),
    )
}

pub struct SVG {
    data: Document,
}

impl SVG {
    pub fn to_file(&self, path: &std::path::Path) -> io::Result<()> {
        ::svg::save(path.with_extension("svg"), &self.data)
    }
    pub fn new(data: Document) -> SVG {
        SVG { data }
    }
}

pub struct IndexBuilder<T>
where
    T: Fn(&usize) -> DVec3,
{
    b: SVGBuilder,
    mapping: T,
}

impl<T> IndexBuilder<T>
where
    T: Fn(&usize) -> DVec3,
{
    pub fn new(b: SVGBuilder, mapping: T) -> Self {
        Self { b, mapping }
    }

    pub fn add_closed_path(&self, to: Group, path: Vec<usize>, stroke: Option<String>) -> Group {
        self.add_path(to, path, true, stroke)
    }
    pub fn add_open_path(&self, to: Group, path: Vec<usize>, stroke: Option<String>) -> Group {
        self.add_path(to, path, false, stroke)
    }

    pub fn add_path(
        &self,
        to: Group,
        path: Vec<usize>,
        closed: bool,
        stroke: Option<String>,
    ) -> Group {
        self.b
            .add_path(to, path.iter().map(&self.mapping).collect(), closed, stroke)
    }
}

pub enum TextAnchor {
    Left,
    Right,
    Center,
}

#[derive(Clone)]
pub struct SVGBuilder {
    pub doc_origin: DVec2,
    pub view_matrix: DMat4,
    pub doc_size: DVec2,
}

impl SVGBuilder {
    pub fn new(doc_size: DVec2, doc_origin: DVec2, view_matrix: DMat4) -> SVGBuilder {
        SVGBuilder {
            doc_origin,
            view_matrix,
            doc_size,
        }
    }

    pub fn translate(&self, delta: DVec2) -> SVGBuilder {
        let mut copy = self.clone();
        copy.doc_origin = copy.doc_origin + delta;
        copy
    }

    pub fn get_svg_coord(&self, draw_coord: DVec2) -> (f64, f64) {
        (
            draw_coord.x + self.doc_origin.x,
            self.doc_size.y - (draw_coord.y + self.doc_origin.y),
        )
    }
    pub fn get_draw_coord(&self, world_coord: DVec3) -> DVec2 {
        let p = self.view_matrix * dvec4(world_coord.x, world_coord.y, world_coord.z, 1.0);
        dvec2(p.x, p.y)
    }

    pub fn add_closed_path(&self, to: Group, path: Vec<DVec3>, stroke: Option<String>) -> Group {
        self.add_path(to, path, true, stroke)
    }
    pub fn add_open_path(&self, to: Group, path: Vec<DVec3>, stroke: Option<String>) -> Group {
        self.add_path(to, path, false, stroke)
    }

    pub fn add_path(
        &self,
        to: Group,
        path: Vec<DVec3>,
        closed: bool,
        stroke: Option<String>,
    ) -> Group {
        let mut data = Data::new();
        let mut first = true;
        for p in path.iter() {
            let p = self.get_svg_coord(self.get_draw_coord(*p));

            if first {
                first = false;
                data = data.move_to(p);
            } else {
                data = data.line_to(p);
            }
        }
        if closed {
            data = data.close();
        }
        to.add(new_path(data, stroke))
    }

    pub fn add_circle(&self, layer: Group, center: DVec3, radius: f64) -> Group {
        let p = self.get_svg_coord(self.get_draw_coord(center));
        layer.add(new_circle(p, radius))
    }

    pub fn add_label(
        &self,
        layer: Group,
        text: &str,
        position: DVec3,
        size_mm: f64,
        align: TextAnchor,
    ) -> Group {
        let p = self.get_svg_coord(self.get_draw_coord(position));
        let mut elem = ::svg::node::element::Text::new()
            .add(::svg::node::Text::new(text))
            .set("font-size", format!("{:.2}px", size_mm))
            .set("x", p.0)
            .set("y", p.1);
        match align {
            TextAnchor::Center => elem = elem.set("text-anchor", "middle"),
            TextAnchor::Right => elem = elem.set("text-anchor", "end"),
            TextAnchor::Left => (),
        }
        layer.add(elem)
    }
}

pub fn new_layer(name: &str) -> Group {
    Group::new()
        .set("inkscape:label", name)
        .set("inkscape:groupmode", "layer")
}

pub fn new_document(size: DVec2) -> ::svg::Document {
    let width = size.x;
    let height = size.y;
    Document::new()
        .set(
            "xmlns:inkscape",
            "http://www.inkscape.org/namespaces/inkscape",
        )
        .set("viewBox", (0, 0, width, height))
        .set("width", format!("{}mm", width))
        .set("height", format!("{}mm", height))
}

pub fn new_path(data: Data, stroke: Option<String>) -> Path {
    let mut p = Path::new()
        .set("fill", "none")
        .set("stroke", "black")
        .set("stroke-width", 0.5);
    if let Some(s) = stroke {
        p = p.set("stroke-dasharray", s);
    }
    p.set("d", data)
}

pub fn new_circle(center: (f64, f64), radius: f64) -> Circle {
    Circle::new()
        .set("cx", center.0)
        .set("cy", center.1)
        .set("r", radius)
        .set("fill", "none")
        .set("stroke", "black")
        .set("stroke-width", 0.5)
}
