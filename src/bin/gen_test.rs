use std::path::{PathBuf};
use svg_drawing::tiny_xml::*;
use svg_drawing::*;
use svg_drawing::SVG::*;

fn main() {
    test1();
    test2();
    test3();
    test4();
    test5();
}

fn test1() {
    // Create XML document
    let mut doc = XML::new();
    let doc = &mut doc;
    let body = XML::ROOT.add_tag(doc, "body");

    body.add_comment(doc, "Testing a comment");
    let node1 = body.add_tag(doc, "test");
    node1.set_attrib(doc, "property", "some-value");

    node1.add_text(doc, "Some Text");
    node1.add_tag(doc, "sub1");

    doc.to_file(&PathBuf::from("bin/xml-test.xml")).unwrap();
}

fn test2() {
    // Create simple SVG Image
    let mut doc = XML::new();

    let (svg, mut doc) = SVG::new(&mut doc, Vec2::new(300.0, 200.0));
    let doc = &mut doc;

    let rect = svg.add_rect(doc, Vec2::new(0.0, 0.0), Vec2::new(300.0, 200.0));
    rect.fill(doc, "red");

    let circle = svg.add_circle(doc, Vec2::new(150.0, 100.0), 80.0);
    circle.fill(doc, "green");
    
    let text = svg.add_svg_text(doc, "SVG", Vec2::new(150.0, 125.0), 60.0, SVG::TextAnchor::Middle);
    text.fill(doc, "white");

    let text_style = |node: &dyn Stylable, doc: &mut DocumentView| {
        node.fill(doc, "black");
        node.stroke(doc, "white");
        node.stroke_width(doc, 0.5);
    };

    
    let group = svg.add_group(doc);
    group.translate(doc, Vec2::new(100.0, 180.0));

    let t = group.add_svg_text(doc, "AAA", Vec2::new(-50.0, 0.0), 20.0, SVG::TextAnchor::Middle);
    text_style(&t, doc);
    let t = group.add_svg_text(doc, "BBB", Vec2::new(0.0, 0.0), 20.0, SVG::TextAnchor::Middle);
    text_style(&t, doc);
    let t = group.add_svg_text(doc, "CCC", Vec2::new(50.0, 0.0), 20.0, SVG::TextAnchor::Middle);
    text_style(&t, doc);
    
    doc.get_xml().to_file(&PathBuf::from("bin/svg-test.svg")).unwrap();
}

fn test3() {
    // Create Inkscape-compatible SVG with layers
    let mut doc = XML::new();

    let (inkscape, mut doc) = INKSCAPE::new(&mut doc, Vec2::new(300.0, 200.0));
    let doc = &mut doc;



    let layer1 = inkscape.add_layer(doc, "Layer 1");

    let rect = layer1.add_rect(doc, Vec2::new(53.67, 42.65), Vec2::new(96.76, 25.70));
    rect.fill(doc, "#ffeed0");
    rect.stroke(doc, "#c43636");
    rect.stroke_width(doc, 1.0);
    rect.round_corner(doc, 0.84);

    let circle = layer1.add_circle(doc, Vec2::new(217.55, 63.61), 12.29);
    circle.fill(doc, "none");
    circle.stroke(doc, "#000000");
    circle.stroke_width(doc, 0.39);
    
    let text = layer1.add_svg_text(doc, "Hello", Vec2::new(63.61, 94.08), 10.58, SVG::TextAnchor::Start);
    text.fill(doc, "#000000");
    text.font_family(doc, "Calibri");



    let layer2 = inkscape.add_layer(doc, "Layer 2");

    let rect2 = layer2.add_rect(doc, Vec2::new(132.21, 85.36), Vec2::new(42.78, 27.87));
    rect2.fill(doc, "#0000ff");
    rect2.round_corner(doc, 0.84);
    
    doc.get_xml().to_file(&PathBuf::from("bin/inkscape-test.svg")).unwrap();
}

fn test4() {
    // Use transformations to draw 3D shapes
    let mut xml = XML::new();
    let (svg, mut doc) = SVG::new_right_hand(&mut xml, Vec2::new(150.0, 150.0));

    // Add a margin to the document
    let mut margin_view = doc.translate(&Vec3::new(10.0, 10.0, 0.0));
    // Add a rectangle in the new view
    let rect = svg.add_rect(&mut margin_view, Vec2::new(0.0, 0.0), Vec2::new(130.0, 130.0));
    rect.stroke_width(&mut margin_view, 1.0);
    rect.stroke(&mut margin_view, "#000000");
    rect.fill(&mut margin_view, "none");

    // Build an isometric view on top the margin view
    use glm::ext::*;
    let matrix = 
        translate(&num::one(), Vec3::new(65.0, 65.0, 0.0)) 
        * scale(&num::one(), Vec3::new(7.0, 7.0, 7.0)) 
        * look_at(Vec3::new(1.0, 1.0, 1.0), Vec3::new(0.0, 0.0, 0.0), Vec3::new(0.0, 0.0, 1.0));
    let mut iso_view = margin_view.transform(&matrix);
    //let mut iso_view = iso_view.translate(&Vec3::new());

    let clamp = |val, min, max| {
        if val < min { min } else if val > max { max } else { val }
    };
    let convert = |val| {
        (clamp(val, 0.0, 1.0) * 255.0) as i32
    };

    let rgb = |color: Vec3| {
        format!("rgb({},{},{})", convert(color.x), convert(color.y), convert(color.z))
    };

    let add_cube = |pos: Vec3, base_color: Vec3, svg: &SVGGroupId, view: &mut DocumentView| {
        let points1 = vec![Vec3::new(1.0, 0.0, 0.0), Vec3::new(1.0, 1.0, 0.0), Vec3::new(1.0, 1.0, 1.0), Vec3::new(1.0, 0.0, 1.0)];
        let points2 = vec![Vec3::new(0.0, 1.0, 0.0), Vec3::new(1.0, 1.0, 0.0), Vec3::new(1.0, 1.0, 1.0), Vec3::new(0.0, 1.0, 1.0)];
        let points3 = vec![Vec3::new(0.0, 0.0, 1.0), Vec3::new(0.0, 1.0, 1.0), Vec3::new(1.0, 1.0, 1.0), Vec3::new(1.0, 0.0, 1.0)];

        let face1 = svg.add_polygon_3d_closed(view, &points1.iter().map(|p| {*p + pos}).collect());
        face1.fill(view, &rgb(base_color));
        
        let face2 = svg.add_polygon_3d_closed(view, &points2.iter().map(|p| {*p + pos}).collect());
        face2.fill(view, &rgb(base_color * 0.75));
        
        let face3 = svg.add_polygon_3d_closed(view, &points3.iter().map(|p| {*p + pos}).collect());
        face3.fill(view, &rgb(base_color * 1.25));
    };

    add_cube(Vec3::new(0.0, 0.0, 0.0), Vec3::new(0.9, 0.4, 0.4), &svg, &mut iso_view);
    add_cube(Vec3::new(-1.0, 0.0, 1.0), Vec3::new(0.4, 0.9, 0.9), &svg, &mut iso_view);
    add_cube(Vec3::new(-1.0, 1.0, 0.0), Vec3::new(0.9, 0.4, 0.9), &svg, &mut iso_view);
    add_cube(Vec3::new(0.0, 1.0, -1.0), Vec3::new(0.4, 0.4, 0.9), &svg, &mut iso_view);

    
    doc.get_xml().to_file(&PathBuf::from("bin/projection-test.svg")).unwrap();
}

// Test <style>
fn test5() {
    let mut doc = XML::new();

    let (svg, mut doc) = SVG::new(&mut doc, Vec2::new(200.0, 350.0));
    let doc = &mut doc;

    svg.styles(doc.get_xml(), vec!{
        Style::new("rect")
            .fill("Aquamarine")
            .stroke_width(2.0),
        Style::new(".test")
            .fill("gold")
            .stroke("maroon")
            .stroke_width(4.0)
            .round_corner(5.0)
    });

    svg.add_rect(doc, Vec2::new(10.0, 10.0), Vec2::new(50.0, 25.0));

    let rect = svg.add_rect(doc, Vec2::new(10.0, 40.0), Vec2::new(50.0, 25.0));
    rect.fill(doc, "none");
    rect.stroke(doc, "#cbcbcb");
    rect.stroke_width(doc, 2.0);

    let rect = svg.add_rect(doc, Vec2::new(10.0, 70.0), Vec2::new(50.0, 25.0));
    rect.class(doc.get_xml(), "test");

    
    doc.get_xml().to_file(&PathBuf::from("bin/style-test.svg")).unwrap();
}