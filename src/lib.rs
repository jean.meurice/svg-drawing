//! # SVG Drawing
//!
//! `svg_drawing` is a crate that can be used to generate SVG files easily and with full control
//! 
//! ## Example Usage
//! 
//! ```rust
//! let mut doc = XML::new();
//! let d = &mut doc;
//! 
//! let svg = SVG::new(d, )
//! ```
//! 

pub mod tiny_xml;

pub use glm::*;

pub mod SVG {
    //pub use SVGContainer;
    use glm::*;
    use glm::ext::*;
    use crate::tiny_xml::*;

    pub struct DocumentView<'a> {
        xml: &'a mut XML,
        transformation: Option<Mat4>,
    }

    impl DocumentView<'_> {
        pub fn get_xml(&mut self) -> &mut XML {
            self.xml
        }
        pub fn get_transformation(&self) -> Option<Mat4> {
            self.transformation.clone()
        }

        fn transform_point_2d(&self, point: &Vec2) -> Vec2 {
            match self.transformation {
                Some(mat) => {
                    let p = Vec4::new(point.x, point.y, 0.0, 1.0);
                    let p = mat * p;
                    Vec2::new(p.x, p.y)
                },
                None => point.to_owned()
            }
        }

        fn transform_point_3d(&self, point: &Vec3) -> Vec2 {
            match self.transformation {
                Some(mat) => {
                    let p = Vec4::new(point.x, point.y, point.z, 1.0);
                    let p = mat * p;
                    Vec2::new(p.x, p.y)
                },
                None => Vec2::new(point.x, point.y)
            }
        }
        
        fn transform_vector_2d(&self, point: &Vec2) -> Vec2 {
            match self.transformation {
                Some(mat) => {
                    let p = Vec4::new(point.x, point.y, 0.0, 0.0);
                    let p = mat * p;
                    Vec2::new(p.x, p.y)
                },
                None => point.to_owned()
            }
        }

        fn transform_vector_3d(&self, point: &Vec3) -> Vec2 {
            match self.transformation {
                Some(mat) => {
                    let p = Vec4::new(point.x, point.y, point.z, 0.0);
                    let p = mat * p;
                    Vec2::new(p.x, p.y)
                },
                None => Vec2::new(point.x, point.y)
            }
        }

        pub fn transform<'a>(&'a mut self, mat: &Mat4) -> DocumentView<'a> {
            let prev_matrix = match &self.transformation {
                Some(t) => t.to_owned(),
                None => num::one(),
            };
            DocumentView { xml: self.xml, transformation: Some(prev_matrix * *mat) }
        }
        pub fn translate<'a>(&'a mut self, vec: &Vec3) -> DocumentView<'a> {
            self.transform(&translate(&num::one(), *vec))
        }
        pub fn scale<'a>(&'a mut self, vec: &Vec3) -> DocumentView<'a> {
            self.transform(&scale(&num::one(), *vec))
        }
        pub fn look_at<'a>(&'a mut self, eye: &Vec3, center: &Vec3, up: &Vec3) -> DocumentView<'a> {
            self.transform(&look_at(*eye, *center, *up))
        }

    }

    
    pub fn new<'a>(doc: &'a mut XML, size: Vec2) -> (SVGGroupId, DocumentView<'a>) {
        let width = size.x;
        let height = size.y;

        let svg = XML::ROOT.add_tag(doc, "svg");

        svg.set_attrib(doc, "version", "1.1");
        svg.set_attrib(doc, "xmlns", "http://www.w3.org/2000/svg");

        svg.set_attrib(doc, "width", &format!("{}mm", width));
        svg.set_attrib(doc, "height", &format!("{}mm", height));
        svg.set_attrib(doc, "viewBox", &format!("0, 0, {}, {}", width, height));

        (SVGGroupId { id: svg.id() }, DocumentView {xml: doc, transformation: None})
    }
    
    /// Creates a new SVG tag and sets the DocumentView to a right-hand rule coordinate system.
    /// The origin is in the bottom left of the document, the X axis points to the right and the Y axis to the top.
    pub fn new_right_hand<'a>(doc: &'a mut XML, size: Vec2) -> (SVGGroupId, DocumentView<'a>) {
        let (svg, _) = new(doc, size);

        let matrix = translate(&scale(&num::one(), Vec3::new(1.0, -1.0, 1.0)), Vec3::new(0.0, -size.y, 0.0));

        (svg, DocumentView {xml: doc, transformation: Some(matrix)})
    }


    pub struct SVGGroupId {
        id: usize,
    }
    impl NodeId for SVGGroupId {
        fn id(&self) -> usize {
            self.id
        }
    }
    impl SVGContainer for SVGGroupId {}
    impl Transformable for SVGGroupId {}

    


    pub trait SVGContainer : NodeId {

        fn add_group(&self, doc: &mut DocumentView) -> SVGGroupId {
            let g = self.add_tag(doc.get_xml(), "g");
            SVGGroupId { id: g.id() }
        }

        fn add_rect(&self, doc: &mut DocumentView, position: Vec2, size: Vec2) -> SVGRectId {
            let rect = self.add_tag(doc.get_xml(), "rect");

            let mut position = doc.transform_point_2d(&position);
            let mut size = doc.transform_vector_2d(&size);
            // Not sure
            if size.x < 0.0 {
                position.x += size.x;
                size.x = -size.x;
            }
            if size.y < 0.0 {
                position.y += size.y;
                size.y = -size.y;
            }

            rect.set_attrib(doc.get_xml(), "x", &position.x.to_string());
            rect.set_attrib(doc.get_xml(), "y", &position.y.to_string());
            rect.set_attrib(doc.get_xml(), "width", &size.x.to_string());
            rect.set_attrib(doc.get_xml(), "height", &size.y.to_string());

            SVGRectId { id: rect.id() }
        }
        
        fn add_circle(&self, doc: &mut DocumentView, center: Vec2, radius: f32) -> SVGCircleId {
            // <circle cx="150" cy="100" r="80" />
            let center = doc.transform_point_2d(&center);

            let circle = self.add_tag(doc.get_xml(), "circle");

            circle.set_attrib(doc.get_xml(), "cx", &center.x.to_string());
            circle.set_attrib(doc.get_xml(), "cy", &center.y.to_string());
            circle.set_attrib(doc.get_xml(), "r", &radius.to_string());

            SVGCircleId { id: circle.id() }
        }
        
        fn add_svg_text(&self, doc: &mut DocumentView, text: &str, position: Vec2, font_size: f32, anchor: TextAnchor) -> SVGTextId {
            // <text x="150" y="125" font-size="60" text-anchor="middle" fill="white">SVG</text>
            
            let position = doc.transform_point_2d(&position);

            let node = self.add_tag(doc.get_xml(), "text");

            node.set_attrib(doc.get_xml(), "xml:space", "preserve");
            node.set_attrib(doc.get_xml(), "x", &position.x.to_string());
            node.set_attrib(doc.get_xml(), "y", &position.y.to_string());
            node.set_attrib(doc.get_xml(), "font-size", &font_size.to_string());
            node.set_attrib(doc.get_xml(), "text-anchor", anchor.get_name());

            node.add_text(doc.get_xml(), text);

            SVGTextId { id: node.id() }
        }

        fn styles(&self, doc: &mut XML, styles: Vec<Style>) {
            let node = self.add_tag(doc, "style");
            let mut res = String::new();
            for s in styles.iter() {
                res += &s.to_string_compact();
            }
            node.add_text(doc, &res);
        }

        fn add_polygon(&self, doc: &mut DocumentView, points: &Vec<Vec2>, closed: bool) -> SVGPathId {
            let mut data = String::new();

            for (i, p) in points.iter().enumerate() {
                let p = doc.transform_point_2d(p);
                if i == 0 {
                    data += &format!("M{},{}", p.x, p.y);
                } else {
                    data += &format!(" L{},{}", p.x, p.y);
                }
            }
            if closed {
                data += " Z";
            }

            let node = self.add_tag(doc.get_xml(), "path");
            node.set_attrib(doc.get_xml(), "d", &data);
            SVGPathId { id: node.id() }
        }
        
        fn add_polygon_3d(&self, doc: &mut DocumentView, points: &Vec<Vec3>, closed: bool) -> SVGPathId {
            let mut data = String::new();

            for (i, p) in points.iter().enumerate() {
                let p = doc.transform_point_3d(p);
                if i == 0 {
                    data += &format!("M{},{}", p.x, p.y);
                } else {
                    data += &format!(" L{},{}", p.x, p.y);
                }
            }
            if closed {
                data += " Z";
            }

            let node = self.add_tag(doc.get_xml(), "path");
            node.set_attrib(doc.get_xml(), "d", &data);
            SVGPathId { id: node.id() }
        }

        fn add_polygon_closed(&self, doc: &mut DocumentView, points: &Vec<Vec2>) -> SVGPathId {
            self.add_polygon(doc, points, true)
        }
        
        fn add_polygon_opened(&self, doc: &mut DocumentView, points: &Vec<Vec2>) -> SVGPathId {
            self.add_polygon(doc, points, false)
        }

        fn add_polygon_3d_closed(&self, doc: &mut DocumentView, points: &Vec<Vec3>) -> SVGPathId {
            self.add_polygon_3d(doc, points, true)
        }
        
        fn add_polygon_3d_opened(&self, doc: &mut DocumentView, points: &Vec<Vec3>) -> SVGPathId {
            self.add_polygon_3d(doc, points, false)
        }

    }


    pub trait Transformable : NodeId {
        fn translate(&self, doc: &mut DocumentView, vec: Vec2) {
            let previous = self.get_attrib(doc.get_xml(), "transform").unwrap_or_default();
            self.set_attrib(doc.get_xml(), "transform", format!("{} translate({} {})", previous, vec.x, vec.y).trim());
        }
    }

    pub trait Stylable : NodeId {
        fn fill(&self, doc: &mut DocumentView, fill: &str) {
            self.set_attrib(doc.get_xml(), "fill", fill);
        }
        fn opacity(&self, doc: &mut DocumentView, opacity: f32) {
            self.set_attrib(doc.get_xml(), "opacity", &opacity.to_string());
        }
        fn stroke(&self, doc: &mut DocumentView, stroke: &str) {
            self.set_attrib(doc.get_xml(), "stroke", stroke);
        }
        fn stroke_width(&self, doc: &mut DocumentView, width: f32) {
            self.set_attrib(doc.get_xml(), "stroke-width", &width.to_string());
        }
    }


    
    pub struct SVGRectId {
        id: usize,
    }
    impl NodeId for SVGRectId {
        fn id(&self) -> usize {
            self.id
        }
    }
    impl Stylable for SVGRectId {}
    impl Transformable for SVGRectId {}
    

    impl SVGRectId {
        pub fn round_corner(&self, doc: &mut DocumentView, radius: f32) {
            self.set_attrib(doc.get_xml(), "ry", &radius.to_string());
        }
    }


    
    pub struct SVGCircleId {
        id: usize,
    }
    impl NodeId for SVGCircleId {
        fn id(&self) -> usize {
            self.id
        }
    }
    impl Stylable for SVGCircleId {}
    impl Transformable for SVGCircleId {}
    
    pub struct SVGTextId {
        id: usize,
    }
    impl NodeId for SVGTextId {
        fn id(&self) -> usize {
            self.id
        }
    }
    impl Stylable for SVGTextId {}
    impl Transformable for SVGTextId {}

    

    impl SVGTextId {
        pub fn font_family(&self, doc: &mut DocumentView, family: &str) {
            self.set_attrib(doc.get_xml(), "font-family", family);
        }
    }

    pub struct SVGPathId {
        id: usize,
    }
    impl NodeId for SVGPathId {
        fn id(&self) -> usize {
            self.id
        }
    }
    impl Stylable for SVGPathId {}
    impl Transformable for SVGPathId {}


    pub enum TextAnchor {
        Start,
        Middle,
        End,
    }
    
    impl TextAnchor {
        fn get_name(&self) -> &str {
            match self {
                TextAnchor::Start => "start",
                TextAnchor::Middle => "middle",
                TextAnchor::End => "end",
            }
        }
    }

    
    pub struct Style {
        selection: String,
        attributes: Vec<(String, String)>,
    }
    impl Style {
        pub fn new(selection: &str) -> Self {
            Style { selection: selection.to_owned(), attributes: Vec::new(), }
        }
        fn to_string_compact(&self) -> String {
            let mut res = String::new();
            res += &self.selection;
            res += "{";
            for (a, v) in self.attributes.iter() {
                res += a;
                res += ":";
                res += v;
                res += ";";
            }
            res += "}";
            res
        }
        fn add(mut self, attribute: &str, value: &str) -> Self {
            self.attributes.push((attribute.to_owned(), value.to_owned()));
            self
        }
        pub fn round_corner(self, radius: f32) -> Self {
            self.add("ry", &radius.to_string())
        }
        pub fn font_family(self, family: &str) -> Self {
            self.add("font-family", family)
        }
        pub fn fill(self, fill: &str) -> Self {
            self.add("fill", fill)
        }
        pub fn stroke(self, stroke: &str) -> Self {
            self.add("stroke", stroke)
        }
        pub fn stroke_width(self, width: f32) -> Self {
            self.add("stroke-width", &width.to_string())
        }
        pub fn font_size(self, font_size: f32) -> Self {
            self.add("font-size", &font_size.to_string())
        }
        pub fn text_anchor(self, anchor: TextAnchor) -> Self {
            self.add("text-anchor", anchor.get_name())
        }
        pub fn stroke_linecap(self, linecap: LineCap) -> Self {
            self.add("stroke-linecap", linecap.to_string())
        }
        pub fn stroke_linejoin(self, linejoin: LineJoin) -> Self {
            self.add("stroke-linejoin", linejoin.to_string())
        }
        pub fn stroke_dasharray(self, dasharray: &str) -> Self {
            self.add("stroke-dasharray", dasharray)
        }
        pub fn stroke_dashoffset(self, offset: f32) -> Self {
            self.add("stroke-dashoffset", &offset.to_string())
        }

    }

    pub enum LineCap {
        Butt,
        Round,
        Square
    }

    impl LineCap {
        pub fn to_string(&self) -> &str {
            match self {
                LineCap::Butt => "butt",
                LineCap::Round => "round",
                LineCap::Square => "square",
            }
        }
    }
    
    pub enum LineJoin {
        Arcs,
        Bevel,
        Miter,
        MiterClip,
        Round,
    }

    impl LineJoin {
        pub fn to_string(&self) -> &str {
            match self {
                LineJoin::Arcs => "arcs",
                LineJoin::Bevel => "bevel",
                LineJoin::Miter => "miter",
                LineJoin::MiterClip => " miter-clip",
                LineJoin::Round => "round",
            }
        }
    }
}

pub mod INKSCAPE {
    use glm::*;
    use crate::{tiny_xml::*, SVG::DocumentView};
    
    /// Creates the base SVG tag inside the given XML, and makes it compatible for Inkscape tags (adds the xmlns:inkscape namespace)
    /// Returns the SVG tag handle
    pub fn new<'a>(doc: &'a mut XML, size: Vec2) -> (InkscapeNodeId, DocumentView<'a>) {
        let (svg, mut view) = crate::SVG::new(doc, size);

        svg.set_attrib(view.get_xml(), "xmlns:inkscape", "http://www.inkscape.org/namespaces/inkscape");
        
        (InkscapeNodeId { id: svg.id() }, view)
    }

    /// Creates the base SVG tag inside the given XML, and makes it compatible for Inkscape tags (adds the xmlns:inkscape namespace)
    /// Returns the SVG tag handle
    /// See SVG::new_right_hand() for the coordinate system
    pub fn new_right_hand<'a>(doc: &'a mut XML, size: Vec2) -> (InkscapeNodeId, DocumentView<'a>) {
        let (svg, mut view) = crate::SVG::new_right_hand(doc, size);

        svg.set_attrib(view.get_xml(), "xmlns:inkscape", "http://www.inkscape.org/namespaces/inkscape");
        
        (InkscapeNodeId { id: svg.id() }, view)
    }



    pub struct InkscapeNodeId {
        id: usize,
    }
    impl NodeId for InkscapeNodeId {
        fn id(&self) -> usize {
            self.id
        }
    }
    
    impl InkscapeNodeId {
        pub fn add_layer(&self, doc: &mut DocumentView, layer_name: &str) -> LayerNodeId {
            let node = self.add_tag(doc.get_xml(), "g");

            node.set_attrib(doc.get_xml(), "inkscape:label", layer_name);
            node.set_attrib(doc.get_xml(), "inkscape:groupmode", "layer");

            LayerNodeId { id: node.id() }
        }
    }



    pub struct LayerNodeId {
        id: usize,
    }
    impl NodeId for LayerNodeId {
        fn id(&self) -> usize {
            self.id
        }
    }
    impl crate::SVG::SVGContainer for LayerNodeId {}
    impl crate::SVG::Transformable for LayerNodeId {}
    

}