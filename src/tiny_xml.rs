
use std::io::Read;
use std::io::{BufReader, Cursor};
use std::io;
use std::path::Path;
use quick_xml::escape;
use quick_xml::events::{BytesDecl, BytesEnd, BytesStart, BytesText, Event};
use quick_xml::Reader;
use quick_xml::Writer;
use std::fs::File;
use std::io::prelude::*;

pub struct XML {
    contents: Vec<Node>,
}


#[derive(PartialEq)]
pub enum NodeType {
    Tag,
    Text,
    Comment,
    CData,
    Decl,
    PI,
    DocType,
    Document,
    Empty, // For when deleted
}

pub struct Node {
    pub typ: NodeType,
    pub name_or_str: String,
    pub keep_start_end: bool, // For nodes without children, whether to still represent the tag as a <start> </end> pair
    pub attribs: Vec<Attribute>,
    pub children: Vec<usize>,
    pub encoding: Option<String>,
    pub standalone: Option<bool>,
}


impl Node {
    fn new(typ: NodeType, txt: String) -> Self {
        Self {
            typ: typ,
            name_or_str: txt,
            keep_start_end: false,
            attribs: Vec::new(),
            children: Vec::new(),
            encoding: None,
            standalone: None,
        }
    }
    fn add_tag(name: &str) -> Self {
        Self {
            typ: NodeType::Tag,
            name_or_str: String::from(name),
            keep_start_end: false,
            attribs: Vec::new(),
            children: Vec::new(),
            encoding: None,
            standalone: None,
        }
    }

    pub fn single(&self) -> bool {
        self.children.len() == 0
    }
}

#[derive(Clone)]
pub struct Attribute {
    key: String,
    value: String,
}


impl XML {
    pub const ROOT: XMLNodeId = XMLNodeId { id: 0 };

    // Warning: use at own risks
    pub fn access(&self, id: usize) -> &Node {
        &self.contents[id]
    }

    pub fn new() -> Self {
        let mut xml = XML {
            contents: Vec::new(),
        };
        xml.contents.push(Node::new(NodeType::Document, String::from("root")));
        xml
    }


    // No "floating" sub-tree for now
    // pub fn add_tag(&mut self, tag_name: &str) -> usize {
    //     let id = self.contents.len();
    //     self.contents.push(Node::add_tag(tag_name));
    //     id
    // }

    // fn new_child(&mut self, parent_id: usize, content: Node) -> usize {
    //     let id = self.contents.len();
    //     self.contents.push(content);
    //     self.contents[parent_id].children.push(id);
    //     id
    // }


    pub fn load<T: Read>(reader: BufReader<T>) -> XML {
        let mut reader = Reader::from_reader(reader);
        reader.trim_text(true);

        let mut contents = Vec::new();
        // Add root (empty name) tag
        contents.push(Node::new(NodeType::Document, String::from("root")));

        let mut buf = Vec::new();
        let mut current_node_stack = Vec::new();
        let mut current_parent = 0;

        loop {
            match reader.read_event(&mut buf) {
                Ok(Event::Start(ref e)) => {
                    let mut tag = Node::add_tag(reader.decode(e.name()).unwrap_or(""));
                    tag.keep_start_end = true;
                    tag.attribs = e
                        .attributes()
                        .map(|a| {
                            let att = a.unwrap();
                            Attribute {
                                key: String::from(reader.decode(att.key).unwrap()),
                                value: att.unescape_and_decode_value(&reader).unwrap(),
                            }
                        })
                        .collect::<Vec<_>>();
                    let id = contents.len();
                    contents.push(tag);
                    contents[current_parent].children.push(id);
                    current_node_stack.push(current_parent);
                    current_parent = id;
                }
                Ok(Event::End(_)) => {
                    current_parent = current_node_stack.pop().unwrap();
                }
                Ok(Event::Empty(ref e)) => {
                    let mut tag = Node::add_tag(reader.decode(e.name()).unwrap_or(""));
                    tag.attribs = e
                        .attributes()
                        .map(|a| {
                            let att = a.unwrap();
                            Attribute {
                                key: String::from(reader.decode(att.key).unwrap()),
                                value: att.unescape_and_decode_value(&reader).unwrap(),
                            }
                        })
                        .collect::<Vec<_>>();
                    let id = contents.len();
                    contents.push(tag);
                    contents[current_parent].children.push(id);
                }
                Ok(Event::Text(e)) => {
                    let id = contents.len();
                    contents[current_parent].children.push(id);
                    contents.push(Node::new(
                        NodeType::Text,
                        e.unescape_and_decode(&reader).unwrap(),
                    ));
                }
                Ok(Event::Comment(e)) => {
                    let id = contents.len();
                    contents[current_parent].children.push(id);
                    contents.push(Node::new(
                        NodeType::Comment,
                        e.unescape_and_decode(&reader).unwrap(),
                    ));
                }
                Ok(Event::CData(e)) => {
                    let id = contents.len();
                    contents[current_parent].children.push(id);
                    contents.push(Node::new(
                        NodeType::CData,
                        e.unescape_and_decode(&reader).unwrap(),
                    ));
                }
                Ok(Event::PI(e)) => {
                    let id = contents.len();
                    contents[current_parent].children.push(id);
                    contents.push(Node::new(
                        NodeType::PI,
                        e.unescape_and_decode(&reader).unwrap(),
                    ));
                }
                Ok(Event::DocType(e)) => {
                    let id = contents.len();
                    contents[current_parent].children.push(id);
                    contents.push(Node::new(
                        NodeType::DocType,
                        e.unescape_and_decode(&reader).unwrap(),
                    ));
                }
                Ok(Event::Decl(ref e)) => {
                    let mut tag = Node::new(
                        NodeType::Decl,
                        String::from(reader.decode(&e.version().unwrap()).unwrap()),
                    );
                    let enc = e.encoding();
                    if let Some(x) = enc {
                        tag.encoding = Some(String::from(reader.decode(&x.unwrap()).unwrap()));
                    }
                    let sta = e.standalone();
                    if let Some(x) = sta {
                        tag.standalone = Some(reader.decode(&x.unwrap()).unwrap() == "yes");
                    }
                    let id = contents.len();
                    contents[current_parent].children.push(id);
                    contents.push(tag);
                }
                Ok(Event::Eof) => break, // exits the loop when reaching end of file
                Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
            }

            // if we don't keep a borrow elsewhere, we can clear the buffer to keep memory usage low
            buf.clear();
        }

        XML { contents }
    }

    pub fn to_file(&self, path: &Path) -> io::Result<()> {
        let mut f = File::create(path)?;
        f.write_all(self.export().as_slice())?;
        Ok(())
    }

    pub fn export(&self) -> Vec<u8> {
        let mut writer = Writer::new(Cursor::new(Vec::new()));

        self.export_intern(&mut writer, 0);
        writer.into_inner().into_inner()
    }

    fn export_intern(&self, writer: &mut Writer<Cursor<Vec<u8>>>, node: usize) {
        let n = &self.contents[node];
        match n.typ {
            NodeType::Tag => {
                if n.single() && !n.keep_start_end {
                    // Write tag
                    let mut elem = BytesStart::owned_name(n.name_or_str.as_bytes());
                    for att in n.attribs.iter() {
                        let t = escape::escape(att.value.as_bytes());
                        let converted_val = std::str::from_utf8(&t).unwrap();
                        elem.push_attribute((&att.key as &str, converted_val));
                    }
                    assert!(writer.write_event(Event::Empty(elem)).is_ok());
                } else {
                    // Write start
                    let mut elem = BytesStart::owned_name(n.name_or_str.as_bytes());
                    for att in n.attribs.iter() {
                        let t = escape::escape(att.value.as_bytes());
                        let converted_val = std::str::from_utf8(&t).unwrap();
                        elem.push_attribute((&att.key as &str, converted_val));
                    }
                    assert!(writer.write_event(Event::Start(elem)).is_ok());

                    // Write children
                    for cid in n.children.iter() {
                        self.export_intern(writer, *cid);
                    }

                    // Write end
                    assert!(writer
                        .write_event(Event::End(BytesEnd::borrowed(n.name_or_str.as_bytes())))
                        .is_ok());
                }
            }
            NodeType::Text => {
                assert!(writer
                    .write_event(Event::Text(BytesText::from_escaped_str(&n.name_or_str)))
                    .is_ok());
            }
            NodeType::Comment => {
                assert!(writer
                    .write_event(Event::Comment(BytesText::from_escaped_str(&n.name_or_str)))
                    .is_ok());
            }
            NodeType::CData => {
                assert!(writer
                    .write_event(Event::CData(BytesText::from_escaped_str(&n.name_or_str)))
                    .is_ok());
            }
            NodeType::Decl => {
                let mut enc = None;
                let mut stand = None;
                if let Some(x) = &n.encoding {
                    enc = Some(x.as_bytes());
                }
                if let Some(x) = &n.standalone {
                    stand = if *x {
                        Some("yes".as_bytes())
                    } else {
                        Some("no".as_bytes())
                    };
                }
                assert!(writer
                    .write_event(Event::Decl(BytesDecl::new(
                        n.name_or_str.as_bytes(),
                        enc,
                        stand
                    )))
                    .is_ok());
            }
            NodeType::PI => {
                assert!(writer
                    .write_event(Event::PI(BytesText::from_escaped_str(&n.name_or_str)))
                    .is_ok());
            }
            NodeType::DocType => {
                assert!(writer
                    .write_event(Event::DocType(BytesText::from_escaped_str(&n.name_or_str)))
                    .is_ok());
            }
            NodeType::Document => {
                for cid in n.children.iter() {
                    self.export_intern(writer, *cid);
                }
            }
            NodeType::Empty => {}
        }
    }
}


pub struct XMLNodeId {
    id: usize,
}

impl NodeId for XMLNodeId {
    fn id(&self) -> usize {
        self.id
    }
}

pub trait NodeId {
    fn id(&self) -> usize;

    fn add_tag(&self, doc: &mut XML, tag_name: &str) -> XMLNodeId {
        let id = doc.contents.len();
        doc.contents.push(Node::add_tag(tag_name));
        doc.contents[self.id()].children.push(id);
        XMLNodeId { id }
    }
    fn add_text(&self, doc: &mut XML, text: &str) -> XMLNodeId {
        let id = doc.contents.len();
        doc.contents.push(Node::new(NodeType::Text, text.into()));
        doc.contents[self.id()].children.push(id);
        XMLNodeId { id }
    }
    fn add_comment(&self, doc: &mut XML, comment: &str) -> XMLNodeId {
        let id = doc.contents.len();
        doc.contents.push(Node::new(NodeType::Comment, comment.into()));
        doc.contents[self.id()].children.push(id);
        XMLNodeId { id }
    }


    fn set_attrib(&self, doc: &mut XML, attrib_name: &str, attrib_value: &str) {
        let node = &mut doc.contents[self.id()];
        if node.typ != NodeType::Tag {
            panic!("Node not a tag");
        }
        for a in node.attribs.iter_mut() {
            if a.key == attrib_name {
                a.value = String::from(attrib_value);
                return;
            }
        }
        node.attribs.push(Attribute {
            key: String::from(attrib_name),
            value: String::from(attrib_value),
        });
    }

    fn remove_attrib(&self, doc: &mut XML, attrib_name: &str) {
        let node = &mut doc.contents[self.id()];
        if node.typ != NodeType::Tag {
            panic!("Node not a tag");
        }
        if let Some(index) = node.attribs.iter().position(|x| x.key == attrib_name) {
            node.attribs.remove(index);
        }
    }

    fn get_first_child_tag(&self, doc: &XML, tag_name: &str) -> Option<XMLNodeId> {
        let node = &doc.contents[self.id()];
        for c in node.children.iter() {
            let n = &doc.contents[*c];
            if n.typ == NodeType::Tag && n.name_or_str == tag_name {
                return Some(XMLNodeId { id: *c });
            }
        }
        None
    }

    fn get_child_tags(&self, doc: &XML, tag_name: &str) -> Vec<XMLNodeId> {
        let node = &doc.contents[self.id()];
        if node.typ != NodeType::Tag {
            return Vec::new();
        }
        let mut res = Vec::new();
        for c in node.children.iter() {
            let n = &doc.contents[*c];
            if n.typ == NodeType::Tag && n.name_or_str == tag_name {
                res.push(XMLNodeId { id: *c });
            }
        }
        res
    }

    fn get_attrib(&self, doc: &mut XML, attrib_name: &str) -> Option<String> {
        let node = &doc.contents[self.id()];
        if node.typ != NodeType::Tag {
            return None;
        }
        for a in node.attribs.iter() {
            if a.key == attrib_name {
                return Some(a.value.clone());
            }
        }
        None
    }

    fn class(&self, doc: &mut XML, class: &str) {
        self.set_attrib(doc, "class", class);
    }
    fn add_class(&self, doc: &mut XML, class: &str) {
        let current = self.get_attrib(doc, "class").unwrap_or_default();
        self.set_attrib(doc, "class", &format!("{} {}", current, class));
    }
}
